# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
export JAVA_HOME=/usr/lib/jvm/java/
export ACS_RETAIN=1

export INTROOT=${HOME}/INTROOT
export ACSDATA=${HOME}/ACSDATA
export ACS_CDB=${HOME}/CDB/

export CLASSPATH=$CLASSPATH:$INTROOT/lib/

. /alma/ACS-JUN2017/ACSSW/config/.acs/.bash_profile.acs
export IDL_PATH="$IDL_PATH -I$INTROOT/idl"
