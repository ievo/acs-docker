FROM centos:7

# Adding CTA ACS repositories
COPY cta-repos/ /etc/yum.repos.d/

RUN yum update -y

# Installing ACS
RUN yum install -y acs2017.6.x86_64
ENV ACS_RETAIN=1

# Installing JAVA
RUN yum install -y java-1.8.0-openjdk java-1.8.0-openjdk-devel
ENV JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.212.b04-0.el7_6.x86_64/

# Installing the development software
RUN yum install -y \
  gcc \
  gcc-c++ \
  gcc-gfortran \
  git \
  subversion \
  autoconf \
  make \
  patch \
  wget \ 
  curl

# Installing ASC dependencies
RUN yum install -y net-tools procmail xterm
# RUN yum install -y gdb rcs zip cvs

# CentOS support during OS discovery with ACS
COPY patches/bash_profile.acs.diff /alma/ACS-JUN2017/ACSSW/config/.acs/
WORKDIR /alma/ACS-JUN2017/ACSSW/config/.acs/
RUN patch < bash_profile.acs.diff
RUN rm bash_profile.acs.diff

# Installing editing tools
RUN yum install -y screen emacs vim file

RUN groupadd -g 1000 almamgr && \
    useradd -g 1000 -u 1000 -d /home/almamgr -m -s /bin/bash almamgr

COPY patches/bashrc.diff /home/almamgr/
WORKDIR /home/almamgr/
RUN patch < bashrc.diff
RUN rm bashrc.diff

COPY payload/CDB/ /home/almamgr/CDB/
COPY payload/ACSDATA/ /home/almamgr/ACSDATA/
COPY payload/INTROOT/ /home/almamgr/INTROOT/

RUN yum install -y tigervnc-server

RUN yum install -y epel-release
RUN yum --enablerepo=epel -y groups install "Xfce"
    
RUN \
    mkdir ~almamgr/.vnc && \
    echo "#!/bin/bash" > ~almamgr/.vnc/xstartup && \
    echo "xrdb $HOME/.Xresources" >> ~almamgr/.vnc/xstartup && \
    echo "startxfce4 &" >> ~almamgr/.vnc/xstartup && \
    chmod +x ~almamgr/.vnc/xstartup && \
    chown -R almamgr:almamgr ~almamgr/.vnc/

# USER almamgr
